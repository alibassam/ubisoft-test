package com.alibassam.ubisoft.restapp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibassam.ubisoft.restapp.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestAppApplicationTests {
	
	@Autowired
	UserRepository userRepository;

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void findAllUsers() {
		userRepository.findAll();
	}

}
