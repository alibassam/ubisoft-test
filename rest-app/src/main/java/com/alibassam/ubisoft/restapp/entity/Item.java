package com.alibassam.ubisoft.restapp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Item Entity
 * @author Ali
 *
 */
@Entity
@Table(name = "item")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Item {

	@Id
	@Column(name = "idItem", precision = 11, scale = 0, nullable = false)
	private Integer idItem;
	@Column(name = "name", length = 45, nullable = true)
	private String name;
	@Column(name = "game", length = 45, nullable = true)
	private String game;
	@Temporal(TemporalType.DATE)
	@Column(name = "expirationDate", length = 45, nullable = true)
	private Date expirationDate;
	@Column(name = "quantity", precision = 11, scale = 0, nullable = true)
	private Integer quantity;
	@ManyToOne
	@JoinColumn(name="idUser", referencedColumnName="idUser", foreignKey=@ForeignKey(name = "FK_idItem_idUser"))
	@XmlTransient
	private User user;
	
	public Integer getIdItem() {
		return idItem;
	}
	public void setIdItem(Integer idItem) {
		this.idItem = idItem;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGame() {
		return game;
	}
	public void setGame(String game) {
		this.game = game;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}