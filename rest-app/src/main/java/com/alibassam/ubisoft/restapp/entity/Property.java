package com.alibassam.ubisoft.restapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Property Entity
 * @author Ali
 *
 */
@Entity
@Table(name = "property")
public class Property {

	@Id
	@Column(name = "idProperty", precision=11, scale=0, nullable=false)
	private Integer idProperty;
	@Column(name = "name", length = 45, nullable=true)
	private String name;
	@Column(name = "value", length = 45, nullable=true)
	private String value;
	@ManyToOne
	@JoinColumn(name="idItem", referencedColumnName="idItem", foreignKey=@ForeignKey(name = "FK_idProperty_idItem"))
	private Item item;
	
	public Integer getIdProperty() {
		return idProperty;
	}
	public void setIdProperty(Integer idProperty) {
		this.idProperty = idProperty;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
}