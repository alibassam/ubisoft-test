package com.alibassam.ubisoft.restapp.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * User Entity
 * @author Ali
 *
 */
@Entity
@Table(name = "user")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User {

	@Id
	@Column(name = "idUser")
	private Integer idUser;
	@Column(name = "username", nullable=false, unique=true)
	private String username;
	@OneToMany(mappedBy="user")
	public Set<Item> items = new HashSet<>();
	
	public User() {
	}
	
	public User(Integer idUser, String username, Set<Item> items) {
		this.idUser = idUser;
		this.username = username;
		this.items = items;
	}

	public Integer getIdUser() {
		return idUser;
	}
	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Set<Item> getItems() {
		return items;
	}
	public void setItems(Set<Item> items) {
		this.items = items;
	}
}

