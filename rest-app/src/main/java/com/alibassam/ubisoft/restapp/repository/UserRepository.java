package com.alibassam.ubisoft.restapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alibassam.ubisoft.restapp.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	/**
	 * Find a user by its username
	 * @param username
	 * @return
	 */
	public User findByUsername(String username);
}
