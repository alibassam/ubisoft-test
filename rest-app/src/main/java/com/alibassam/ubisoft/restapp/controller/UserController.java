package com.alibassam.ubisoft.restapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.alibassam.ubisoft.restapp.entity.User;
import com.alibassam.ubisoft.restapp.repository.UserRepository;

/**
 * User services controller
 * @author Ali
 *
 */
@RestController
public class UserController {
	
	@Autowired
	UserRepository userRepository;

	/**
	 * Retrieves the items of a user by providing a username
	 * @param username
	 * @return
	 */
	@GetMapping(path="/user/{username}", produces=MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<User> getUserItems(@PathVariable("username") String username) {
		//Find the user by the username
		User user = userRepository.findByUsername(username);
		//If user is not found, return 204
		if(user == null)
			return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
		
		//Return user with status 200
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
}
