package com.alibassam.ubisoft.restapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alibassam.ubisoft.restapp.entity.Property;

public interface PropertyRepository extends JpaRepository<Property, Integer> {

}
