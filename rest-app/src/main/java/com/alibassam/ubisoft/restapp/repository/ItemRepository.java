package com.alibassam.ubisoft.restapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alibassam.ubisoft.restapp.entity.Item;

public interface ItemRepository extends JpaRepository<Item, Integer>{

}
