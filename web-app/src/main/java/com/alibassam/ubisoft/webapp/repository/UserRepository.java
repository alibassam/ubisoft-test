package com.alibassam.ubisoft.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alibassam.ubisoft.webapp.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

}
