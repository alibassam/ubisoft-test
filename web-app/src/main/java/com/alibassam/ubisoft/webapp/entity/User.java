package com.alibassam.ubisoft.webapp.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alibassam.ubisoft.webapp.model.Item;

/**
 * User Entity, each user has a single role
 * @author Ali
 *
 */
@Entity
@Table(name = "user")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User {

	@Id
	@Column(name = "idUser", precision = 11, scale = 0, nullable = false)
	private Integer idUser;
	@Column(name = "username", length = 45, nullable = true)
	private String username;
	@Column(name = "password", length = 45, nullable = true)
	private String password;
	@ManyToOne
	@JoinColumn(name="idRole", referencedColumnName="idRole", foreignKey=@ForeignKey(name = "FK_idUser_idRole"))
	private Role role;
	@Transient
	private Set<Item> items = new HashSet<>();
	
	public Integer getIdUser() {
		return idUser;
	}
	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	@Transient
	public Set<Item> getItems() {
		return items;
	}
	@Transient
	public void setItems(Set<Item> items) {
		this.items = items;
	}
}
