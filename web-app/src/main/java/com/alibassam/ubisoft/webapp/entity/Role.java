package com.alibassam.ubisoft.webapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Role Entity
 * @author Ali
 *
 */
@Entity
@Table(name = "role")
public class Role {

	@Id
	@Column(name = "idRole", precision = 11, scale = 0, nullable = false)
	private Integer idRole;
	@Column(name = "roleDescription", length = 45, nullable = true)
	private String roleDescription;
	
	public Integer getIdRole() {
		return idRole;
	}
	public void setIdRole(Integer idRole) {
		this.idRole = idRole;
	}
	public String getRoleDescription() {
		return roleDescription;
	}
	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}
}
