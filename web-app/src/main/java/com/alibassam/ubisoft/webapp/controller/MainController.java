package com.alibassam.ubisoft.webapp.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.alibassam.ubisoft.webapp.entity.User;
import com.alibassam.ubisoft.webapp.repository.UserRepository;

@Controller
public class MainController {
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	UserRepository userRepository;
	
	@GetMapping(path = "/")
	public ModelAndView get(Authentication authentication, ModelAndView mav) {
		//If it's a guest user, retrieve items
		if(authentication.getAuthorities().size() > 0 && "2".equals(authentication.getAuthorities().iterator().next().getAuthority())) {
			//Call the rest app
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
			
			HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

			ResponseEntity<User> response = restTemplate.exchange("/user/" + authentication.getName(), HttpMethod.GET, entity, User.class);
			mav.addObject("userItems", response.getBody());
		}
		
		return defaultModelAndView(mav);
	}
	
	@PostMapping(path = "/")
	public ModelAndView getItems(@ModelAttribute("selectedUsername") String selectedUsername, ModelAndView mav) {
		if(!StringUtils.isEmpty(selectedUsername)) {
			//Call the rest app
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
			
			HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

			ResponseEntity<User> response = restTemplate.exchange("/user/" + selectedUsername, HttpMethod.GET, entity, User.class);
			mav.addObject("userItems", response.getBody());
		}
		
		return defaultModelAndView(mav);
	}
	
	private ModelAndView defaultModelAndView( ModelAndView mav) {
		//Retrieve all users
		List<User> usersList = userRepository.findAll();
		//Add users to model
		mav.addObject("usersList", usersList);
		
		mav.setViewName("items");
		return mav;
	}
}
