package com.alibassam.ubisoft.webapp;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;
	
	/**
	 * Configure path related security
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//Disable CSRF
		http.csrf().disable();
		
		//All Requests must be authenticated
		http.authorizeRequests()
			.anyRequest().authenticated()
			.and()
			.formLogin().loginPage("/login")
			.usernameParameter("username")
			.passwordParameter("password")
			.permitAll()
			.and()
			.logout().permitAll();
	}
	
	/**
	 * Configure JDBC security
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication()
			//Configure queries for JDBC authentication
			.usersByUsernameQuery("select username, password, 'true' as enabled from user where username=?")
			.authoritiesByUsernameQuery("select u.username, r.idRole from user u, role r where u.username=? and u.idRole = r.idRole")
			.dataSource(dataSource)
			.passwordEncoder(passwordEncoder());
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}
}
